import unittest

from src.parser import Parser


class ParserTest(unittest.TestCase):
    def testFixtureOne(self):
        parser = Parser()

        fixture1 = parser.fixture1()
        (address, suite, postcode, description, images) = parser.parse(fixture1)

        expectedAddress = "Crown House, Toutley Road, Wokingham, Berkshire"
        self.assertEquals(address, expectedAddress)
        self.assertEquals(postcode, "RG41 1QW")
        self.assertEquals(suite, "Suite 2")

        # BONUS:
        self.assertEqual(description, "This is some test description 1")

        # face to face interview
        expected_images = [
            'http://media.clarkscomputers.co.uk/images/PHBRA/208042_1_003.jpg',
            'http://media.clarkscomputers.co.uk/images/PHBRA/208042_1_004.jpg'
        ]
        self.assertEqual(images, expected_images)

    def testFixtureTwo(self):
        parser = Parser()

        fixture2 = parser.fixture2()
        (address, suite, postcode, description, images) = parser.parse(fixture2)

        expectedAddress = "329 bracknell, Doncastle Road, Bracknell, Berkshire"
        self.assertEquals(address, expectedAddress)
        self.assertEquals(postcode, None)
        self.assertEquals(suite, None)

        # BONUS:
        self.assertEqual(description, "Description part 1. Desc part 2.")

        # face to face interview
        expected_images = [
            'http://media.clarkscomputers.co.uk/images/PHBRA/217018_1_001.jpg',
            'http://media.clarkscomputers.co.uk/images/PHBRA/217018_1_000.jpg',
            'http://media.clarkscomputers.co.uk/images/PHBRA/217018_1_002.jpg',
            'http://media.clarkscomputers.co.uk/images/PHBRA/217018_1_003.jpg'
        ]
        self.assertEqual(images, expected_images)
